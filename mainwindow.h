#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include "combinationsolver.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void ColorSolution();
    ~MainWindow();

    static const Qt::GlobalColor S_COOK_MAIN_COL() { return Qt::yellow; }
    static const Qt::GlobalColor S_COOK_DESERT_COL() { return Qt::green; }
    static const Qt::GlobalColor S_COOK_STARTER_COL() { return Qt::red; }

private:
    Ui::MainWindow *ui;
    void FillTable(QTableWidget *widget, QVector<int> *values, int teamsCount);
    void FillTable(QTableWidget *widget, QVector<double> *values, int teamsCount);

    CombinationSolver* solver;

private slots:
    void calculateCombinations();
    void newTeams();
};

#endif // MAINWINDOW_H
