#ifndef COMBINATIONSOLVER_H
#define COMBINATIONSOLVER_H

#include <QMatrix>
#include <QLinkedList>
#include <QDebug>
#include <QPair>
#include <QTime>
#include <QtMath>
#include <QFuture>
#include <QtConcurrent>

struct Meal {
    int host;
    int guestOne;
    int guestTwo;
    double score;
    int type;
    QSet<int> teamsWithoutHostedMeal;
};

class CombinationSolver
{
public:
    CombinationSolver(int teamsCount, int pathIndex, bool sortTeamsByXLocation);

    static const int COORDS_MAX() { return 100; }
    static const int S_COOK_STARTER() { return 1; }
    static const int S_COOK_MAIN() { return 2; }
    static const int S_COOK_DESERT() { return 3; }
    static const int S_GUEST() { return 10; }
    static const int S_EMPTY() { return 0; }
    static const int S_INF() { return 999999; }
    static const int M_GUEST_AND_HOST() { return 999; }
    static const int M_MULTIPLE_MEETING() { return 500; }
    static const int M_NOT_HOST_YET() { return 100; }

    QVector<double> GetWeightMatrix();
    QLinkedList<QVector<int>> GetCombinations();
    QVector<QPair<double, double> > GetCoords();
    QString GetStringFromSolution(QVector<double> solution);

    double Solve();
    double GetTravelDistance();

    int getTeamsCount() const;

    QVector<int>* getSolution();

    int getUsedCores() const;
    void setUsedCores(int value);

private:
    double GetRandomCoord(double range);
    double GetDistanceBetweenTeams(QPair<double, double> coordsFromA, QPair<double, double> coordsFromB);
    QSet<int> GetAlreadyMetTeams(int teamIndex);
    void Init();

    QVector<double> currentWeights;

    int teamsCount = 9;
    int usedCores = 0;

    QVector<QPair<double, double> > coords;
    QVector<double> weights;
    QVector<int> solution;
    bool isValidToSolve = false;
    void FindMeals(int type, QVector<int> *currentKitchenPlace, QVector<double>* currentWeights, QVector<Meal> *usedMeals, double* totalDistance);
    QSet<int> GetTeamsWithoutHostedMeal();
    QSet<int> GetTeamsWithoutMeal(int type);
    QSet<int> GetTeamsWithoutMealAsHost(int type);
    int GetMealsCountFromTeam(int team);
    QSet<int> GetMetTeamsByTeam(int team);
    void CalculateNewWeights(QVector<double> *mainWeights, QVector<int> *currentKitchenPlace);

    //double CalcScoreForConstellation(Meal &meal, QSet<int>* teamsWithoutHostedMeal);
    void calcScoreFromMeal(Meal &meal);
    //Meal reduceToBestMeal(Meal &bestMeal, const Meal& mealToCompare);
};

#endif // COMBINATIONSOLVER_H
