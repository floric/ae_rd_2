#include "combinationsolver.h"

CombinationSolver::CombinationSolver(int teamsCount, int pathIndex, bool sortTeamsByXLocation)
{
    // catch wrong number of teams; not multiple of 3
    if(teamsCount % 3 != 0) {
        qDebug() << "Number of teams not multiple of 3!";
        return;
    } else {
        this->teamsCount = teamsCount;
        this->usedCores = pathIndex;
        isValidToSolve = true;
    }

    // generate random coordinates for every team
    coords = QVector<QPair<double, double> >();

    for (int i = 0; i < getTeamsCount(); ++i) {
        QPair<double, double> pair = QPair<double, double>(GetRandomCoord(COORDS_MAX()), GetRandomCoord(COORDS_MAX()));

        coords.append(pair);
    }

    if(sortTeamsByXLocation) {
        qSort(coords);
    }

    Init();
}

QVector<double> CombinationSolver::GetWeightMatrix()
{
    return weights;
}

QString CombinationSolver::GetStringFromSolution(QVector<double> solution)
{
    QString str;

    for (int x = 0; x < solution.size(); ++x) {
        str += QString::number(solution.at(x));
        str += ", ";

        if((x + 1) % getTeamsCount() == 0) {
            str += "\n";
        }

    }

    return str;
}

QSet<int> CombinationSolver::GetTeamsWithoutHostedMeal()
{
    QSet<int> teamsWithoutHost;

    for (int hostIndex = 0; hostIndex < teamsCount; ++hostIndex) {
        if(solution[hostIndex * teamsCount + hostIndex] == S_EMPTY()) {
            teamsWithoutHost.insert(hostIndex);
        }
    }

    return teamsWithoutHost;
}

QSet<int> CombinationSolver::GetTeamsWithoutMeal(int type)
{
    // create set with all teams and remove found teams
    QSet<int> teams;
    for (int teamIndex = 0; teamIndex < teamsCount; ++teamIndex) {
        teams.insert(teamIndex);
    }

    // search for every team the meal
    for (int teamIndex = 0; teamIndex < teamsCount; ++teamIndex) {
        if(solution[teamIndex * teamsCount + teamIndex] == type) {
            teams.remove(teamIndex);

        } else {
            for (int lineIndex = 0; lineIndex < teamsCount; ++lineIndex) {
                if(solution[lineIndex * teamsCount + teamIndex] != S_EMPTY()) {
                    if(solution[lineIndex * teamsCount + lineIndex] == type) {
                        teams.remove(teamIndex);
                        break;
                    }
                }
            }
        }
    }

    //qDebug() << "Teams:" << teams << "have no: " << type;

    return teams;
}

QSet<int> CombinationSolver::GetTeamsWithoutMealAsHost(int type)
{
    QSet<int> teams;
    for (int teamIndex = 0; teamIndex < teamsCount; ++teamIndex) {
        int assignedMeals = 0;

        for (int lineIndex = 0; lineIndex < teamsCount; ++lineIndex) {
            if(solution[lineIndex * teamsCount + teamIndex] != S_EMPTY()) {
                assignedMeals++;
            }
        }

        if(assignedMeals != type) {
            teams.insert(teamIndex);
        }
    }

    return teams;
}

int CombinationSolver::GetMealsCountFromTeam(int team)
{
    int result = 0;

    for (int lineIndex = 0; lineIndex < teamsCount; ++lineIndex) {
        if(solution[lineIndex * teamsCount + team] != S_EMPTY()) {
            result++;
        }
    }

    return result;
}

QSet<int> CombinationSolver::GetMetTeamsByTeam(int team)
{
    QSet<int> teams;

    for (int lineIndex = 0; lineIndex < teamsCount; ++lineIndex) {
        if(solution[lineIndex * teamsCount + team] != S_EMPTY()) {
            if(lineIndex != team) {
                teams.insert(lineIndex);
            }
        }
    }

    return teams;
}

void CombinationSolver::FindMeals(int type, QVector<int>* currentKitchenPlace, QVector<double>* currentWeights, QVector<Meal>* usedMeals, double* totalDistance)
{
    // calculate next meals by generating all possibilities and choosing the ones with the lowest scores
    for(int mealIndex = 0; mealIndex < (teamsCount / 3); ++mealIndex) {
        // get all scores for all lines

        QVector<Meal> possibilities;
        QSet<int> teamsWithoutHostedMeal = GetTeamsWithoutHostedMeal();
        QSet<int> teamsWithoutMealAsGuest = GetTeamsWithoutMeal(type);
        teamsWithoutHostedMeal.intersect(teamsWithoutMealAsGuest);

        // create next possibilities
        for (int teamIndex : teamsWithoutHostedMeal) {
            for(int guestOne : teamsWithoutMealAsGuest) {
                for(int guestTwo : teamsWithoutMealAsGuest) {
                    if(teamIndex != guestOne && teamIndex != guestTwo && guestOne < guestTwo) {

                        Meal m;
                        m.type = type;
                        m.host = teamIndex;
                        m.guestOne = guestOne;
                        m.guestTwo = guestTwo;
                        m.teamsWithoutHostedMeal = teamsWithoutHostedMeal;
                        m.score = currentWeights->at(teamIndex * teamsCount + guestOne) + currentWeights->at(teamIndex * teamsCount + guestTwo);

                        possibilities.append(m);

                        //qDebug() << "H(" << m.type << "): Host" << m.host << "with Guests" << m.guestOne << "&" << m.guestTwo << "; C:" << m.cost;
                    }
                }
            }
        }

        // if there are teams left which haven't cooked or had no last meal
        // then add these options manually and take the cheapest ones
        if(possibilities.isEmpty()) {
            qDebug() << "No possibilities left! Create options with doubled meeting of the same teams.";

            QSet<int> teamsWithoutHost = GetTeamsWithoutHostedMeal();
            QSet<int> teamsWithoutMeal = GetTeamsWithoutMeal(type);

            qDebug() << "Found these teams without host:" << teamsWithoutHost;
            qDebug() << "Found these teams without meal" << type << ":" << teamsWithoutMeal;

            return;
        }

        // calculate scores parallel
        //qDebug() << "START Expensive with" << possibilities.size();
        QtConcurrent::blockingMap(possibilities, [this] (Meal &meal) { calcScoreFromMeal(meal); });
        //qDebug() << "END Expensive!";

        // take best possibility with Greedy approach
        // except on last run for the desert:
        // skip the solutions where a team which hasn't cooked yet gets selected as a guest again
        double currentVal = S_INF() * 10;
        int bestMealIndex = -1;

        // find meal with lowest cost
        for (int x = 0; x < possibilities.size(); ++x) {
            Meal generatedMeal = possibilities.at(x);

            if(generatedMeal.score < currentVal) {
                bestMealIndex = x;
                currentVal = generatedMeal.score;
            }
        }

        if(bestMealIndex == -1) {
            qDebug() << "Run out of options!";

            return;
        }

        // create new meal and add used teams to ignore list for next run for more meals
        Meal mToUse = possibilities.at(bestMealIndex);
        usedMeals->append(mToUse);

        // add distance to travel
        *totalDistance = mToUse.score + *totalDistance;

        // mark solution in the solutionmatrix
        solution[mToUse.host * teamsCount + mToUse.guestOne] = solution[mToUse.host * teamsCount + mToUse.guestTwo] = S_GUEST();
        solution[mToUse.host * teamsCount + mToUse.host] = type;

        // set new locations for guests
        (*currentKitchenPlace)[mToUse.guestOne] = mToUse.host;
        (*currentKitchenPlace)[mToUse.guestTwo] = mToUse.host;
        (*currentKitchenPlace)[mToUse.host] = mToUse.host;

        //qDebug() << "Added solution:";
        //qDebug() << "H" << mToUse.type << ":" << mToUse.host << "with" << mToUse.guestOne << "&" << mToUse.guestTwo << "; C:" << mToUse.cost;
    }
}

void CombinationSolver::CalculateNewWeights(QVector<double>* mainWeights, QVector<int>* currentKitchenPlace)
{
    int teamIndex = 0;
    int guestIndex = 0;

    for (int x = 0; x < mainWeights->size(); ++x) {
        double distance = GetDistanceBetweenTeams(coords.at((*currentKitchenPlace)[teamIndex]), coords.at(guestIndex));
        (*mainWeights)[x] = distance;
        guestIndex++;

        if((x + 1) % getTeamsCount() == 0) {
            teamIndex++;
            guestIndex = 0;
        }
    }
}

void CombinationSolver::calcScoreFromMeal(Meal &meal)
{
    // make sure every team without being host doesn't get assigned as a guest in the last run
    if(meal.type == S_COOK_DESERT()) {
        if(meal.teamsWithoutHostedMeal.contains(meal.guestOne) || meal.teamsWithoutHostedMeal.contains(meal.guestTwo)) {
            meal.score = S_INF();
            return;
        }
    } else if(meal.type == S_COOK_MAIN()) {
        if(meal.teamsWithoutHostedMeal.contains(meal.guestOne) && meal.teamsWithoutHostedMeal.contains(meal.guestTwo)) {
            meal.score = S_INF();
            return;
        }
    }

    if(solution[meal.guestOne * teamsCount + meal.host] != S_EMPTY()) {
        meal.score += M_MULTIPLE_MEETING();
    }
    if(solution[meal.guestTwo * teamsCount + meal.host] != S_EMPTY()) {
        meal.score += M_MULTIPLE_MEETING();
    }

    QSet<int> meetByGuestOne = GetAlreadyMetTeams(meal.guestOne);
    QSet<int> meetByGuestTwo = GetAlreadyMetTeams(meal.guestTwo);

    // add higher score for teams which have already met before
    int meetingsBetweenPeople = 0;
    if(meetByGuestOne.contains(meal.host)) {
        meetingsBetweenPeople++;
    }
    if(meetByGuestOne.contains(meal.guestTwo)) {
        meetingsBetweenPeople++;
    }
    if(meetByGuestTwo.contains(meal.host)) {
        meetingsBetweenPeople++;
    }
    if(meetByGuestTwo.contains(meal.guestOne)) {
        meetingsBetweenPeople++;
    }
    meal.score += M_MULTIPLE_MEETING() * meetingsBetweenPeople;
}

/*void CombinationSolver::reduceToBestMeal(Meal &bestMeal, Meal &mealToCompare)
{

}*/

double CombinationSolver::Solve()
{
    double totalDistance = 0;

    QThreadPool::globalInstance()->setMaxThreadCount(usedCores);

    if(isValidToSolve) {
        QTime time;
        time.start();

        qDebug() << "Solve!";

        Init();

        // create array for current kitchen places
        QVector<int> currentKitchenPlace;
        for(int i = 0; i < teamsCount; ++i) {
            currentKitchenPlace.append(i);
        }

        QVector<Meal> usedMeals;

        QVector<double> changingWeights = QVector<double>(weights);
        CalculateNewWeights(&changingWeights, &currentKitchenPlace);
        FindMeals(S_COOK_STARTER(), &currentKitchenPlace, &changingWeights, &usedMeals, &totalDistance);

        // --------------------- MAIN --------------------

        // calculate new weight
        // distance from kitchen to next possibility
        CalculateNewWeights(&changingWeights, &currentKitchenPlace);
        FindMeals(S_COOK_MAIN(), &currentKitchenPlace, &changingWeights, &usedMeals, &totalDistance);

        // --------------------- DESERT --------------------

        // calculate new weight
        // distance from kitchen to next possibility
        CalculateNewWeights(&changingWeights, &currentKitchenPlace);
        FindMeals(S_COOK_DESERT(), &currentKitchenPlace, &changingWeights, &usedMeals, &totalDistance);

        int timeElapsed = time.elapsed();
        qDebug() << "Took:" << timeElapsed << "ms" << "with" << usedCores << "cores!";
    } else {
        qDebug() << "Solver input invalid!";
    }

    qDebug() << "Total distance:" << GetTravelDistance();


    return totalDistance;
}

double CombinationSolver::GetTravelDistance()
{
    double totalDistance = 0;
    for (int teamIndex = 0; teamIndex < teamsCount; ++teamIndex) {
        double teamDistance = 0;
        int oldLocation = teamIndex;

        for (int lineIndex = 0; lineIndex < teamsCount; ++lineIndex) {
            if(solution[lineIndex * teamsCount + teamIndex] != S_EMPTY()) {
                if(solution[lineIndex * teamsCount + lineIndex] == S_COOK_STARTER()) {
                    teamDistance += GetDistanceBetweenTeams(coords.at(oldLocation), coords.at(lineIndex));
                    oldLocation = lineIndex;
                    break;
                }
            }
        }

        for (int lineIndex = 0; lineIndex < teamsCount; ++lineIndex) {
            if(solution[lineIndex * teamsCount + teamIndex] != S_EMPTY()) {
                if(solution[lineIndex * teamsCount + lineIndex] == S_COOK_MAIN()) {
                    teamDistance += GetDistanceBetweenTeams(coords.at(oldLocation), coords.at(lineIndex));
                    oldLocation = lineIndex;
                    break;
                }
            }
        }

        for (int lineIndex = 0; lineIndex < teamsCount; ++lineIndex) {
            if(solution[lineIndex * teamsCount + teamIndex] != S_EMPTY()) {
                if(solution[lineIndex * teamsCount + lineIndex] == S_COOK_DESERT()) {
                    teamDistance += GetDistanceBetweenTeams(coords.at(oldLocation), coords.at(lineIndex));
                    oldLocation = lineIndex;
                    break;
                }
            }
        }

        totalDistance += teamDistance;
    }

    return totalDistance;
}

double CombinationSolver::GetRandomCoord(double range)
{
    return (((double)rand()/(double)RAND_MAX) * range);
}

double CombinationSolver::GetDistanceBetweenTeams(QPair<double, double> coordsFromA, QPair<double, double> coordsFromB)
{
    double avgX = coordsFromA.first - coordsFromB.first;
    double avgY = coordsFromA.second - coordsFromB.second;

    return (qFabs(qSqrt(avgX * avgX + avgY * avgY)));
}

QSet<int> CombinationSolver::GetAlreadyMetTeams(int teamIndex)
{
    QSet<int> meetTeams;
    for (int otherTeamIndex = 0; otherTeamIndex < teamsCount; ++otherTeamIndex) {
        int meetingVal = solution[otherTeamIndex * teamsCount + teamIndex];

        // find hosts where the team is invited
        if(meetingVal != S_EMPTY()) {
            meetTeams.insert(otherTeamIndex);

            // now find all other guests attending the same meal
            for (int guestIndex = 0; guestIndex < teamsCount; ++guestIndex) {
                int guestVal = solution[otherTeamIndex * teamsCount + guestIndex];
                if(guestVal == S_GUEST()) {
                    meetTeams.insert(guestIndex);
                }
            }
        }
    }

    return meetTeams;
}

void CombinationSolver::Init()
{
    // generate weight matrix
    weights = QVector<double>(getTeamsCount() * getTeamsCount(), 0);

    // fill with distances to other teams
    int teamIndex = 0;
    int guestIndex = 0;

    // calculate weights
    for (int x = 0; x < weights.size(); ++x) {
        if(teamIndex != guestIndex) {
            double distance = GetDistanceBetweenTeams(coords.at(teamIndex), coords.at(guestIndex));
            weights[x] = distance;
        }

        guestIndex++;

        if((x + 1) % getTeamsCount() == 0) {
            teamIndex++;
            guestIndex = 0;
        }
    }

    solution.clear();

    // initialize empty solution matrix
    for (int i = 0; i < (teamsCount * teamsCount); ++i) {
        solution.append(S_EMPTY());
    }
}

int CombinationSolver::getUsedCores() const
{
    return usedCores;
}

void CombinationSolver::setUsedCores(int value)
{
    usedCores = value;
}

QVector<int>* CombinationSolver::getSolution()
{
    return &solution;
}

int CombinationSolver::getTeamsCount() const
{
    return teamsCount;
}
