#ifndef MEAL_H
#define MEAL_H


class Meal
{
public:
    Meal(int host, int guestOne, int guestTwo, double cost, int type);

    int getGuestOne() const;
    void setGuestOne(int value);

    int getGuestTwo() const;
    void setGuestTwo(int value);

    int getHost() const;
    void setHost(int value);

    double getCost() const;
    void setCost(double value);

    int getType() const;
    void setType(int value);

private:
    int host = 0;
    int guestOne = 0;
    int guestTwo = 0;
    double cost = 0;
    int type = 0;
};

#endif // MEAL_H
