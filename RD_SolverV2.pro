#-------------------------------------------------
#
# Project created by QtCreator 2015-12-21T12:50:23
#
#-------------------------------------------------

QT       += core gui concurrent
CONFIG += C++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RD_SolverV2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    combinationsolver.cpp \
    meal.cpp

HEADERS  += mainwindow.h \
    combinationsolver.h \
    meal.h

FORMS    += mainwindow.ui
