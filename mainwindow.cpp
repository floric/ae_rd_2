#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->calculateButton, SIGNAL(clicked(bool)), this, SLOT(calculateCombinations()));
    connect(ui->newTeamsButton, SIGNAL(clicked(bool)), this, SLOT(newTeams()));

    solver = new CombinationSolver(ui->teamsSpinBox->value(), ui->greedySpinBox->value(), false);
}

void MainWindow::ColorSolution()
{
    int teamCount = ui->teamsSpinBox->value();
    QTableWidget* solutionTable = ui->starterCombiTable;
    if(solutionTable->columnCount() == teamCount && solutionTable->rowCount() == teamCount) {
        // color meals
        for (int x = 0; x < teamCount; ++x) {
            for (int y = 0; y < teamCount; ++y) {
                QTableWidgetItem* cell = solutionTable->item(x, y);
                QString cellVal = cell->text();

                if(cellVal.compare(QString::number(CombinationSolver::S_COOK_DESERT())) == 0) {
                    cell->setBackground(S_COOK_DESERT_COL());
                } else  if(cellVal.compare(QString::number(CombinationSolver::S_COOK_MAIN())) == 0) {
                    cell->setBackground(S_COOK_MAIN_COL());
                } else  if(cellVal.compare(QString::number(CombinationSolver::S_COOK_STARTER())) == 0) {
                    cell->setBackground(S_COOK_STARTER_COL());
                }

                QTableWidgetItem* otherCell = solutionTable->item(y, x);
                QString otherCellVal = otherCell->text();

                if(cellVal.compare(QString::number(CombinationSolver::S_EMPTY())) != 0 &&
                        otherCellVal.compare(QString::number(CombinationSolver::S_EMPTY())) != 0 && (x != y)) {

                    cell->setBackground(Qt::gray);
                    otherCell->setBackground(Qt::gray);
                }
            }
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::FillTable(QTableWidget *widget, QVector<int> *values, int teamsCount)
{
    widget->setColumnCount(teamsCount);
    widget->setRowCount(teamsCount);

    int teamIndex = 0;
    int guestIndex = 0;
    for (int x = 0; x < values->size(); ++x) {
        QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(values->at(x)));
        widget->setItem(teamIndex, guestIndex, newItem);
        guestIndex++;

        if((x + 1) % teamsCount == 0) {
            teamIndex++;
            guestIndex = 0;
        }
    }

    ColorSolution();
}

void MainWindow::FillTable(QTableWidget *widget, QVector<double> *values, int teamsCount)
{
    widget->setColumnCount(teamsCount);
    widget->setRowCount(teamsCount);

    int teamIndex = 0;
    int guestIndex = 0;
    for (int x = 0; x < values->size(); ++x) {
        QTableWidgetItem *newItem = new QTableWidgetItem(QString::number(values->at(x)));
        widget->setItem(teamIndex, guestIndex, newItem);
        guestIndex++;

        if((x + 1) % teamsCount == 0) {
            teamIndex++;
            guestIndex = 0;
        }
    }
}

void MainWindow::calculateCombinations()
{
    qDebug() << "Ideal core count:" << QThread::idealThreadCount();
    double score = 0;

    for (int coreCount = ui->greedySpinBox->value(); coreCount >= 1; --coreCount) {
        for(int run = 1; run <= 10; ++run) {
            qDebug() << "Run:" << run << " with " << coreCount << "cores!";
            solver->setUsedCores(coreCount);

            score = solver->Solve();
        }
    }

    const int teamsCount = solver->getTeamsCount();
    QVector<double> weights = solver->GetWeightMatrix();

    // fill tables
    QVector<int>* solution = solver->getSolution();

    if(solution->size() <= 900) {
        FillTable(ui->weightsTable, &weights, teamsCount);
        FillTable(ui->starterCombiTable, solution, teamsCount);
    }

    ui->scoreLabel->setText(QString::number(score));
    ui->distanceLabel->setText(QString::number(solver->GetTravelDistance()));
}

void MainWindow::newTeams()
{
    delete solver;
    solver = new CombinationSolver(ui->teamsSpinBox->value(), ui->greedySpinBox->value(), false);
    calculateCombinations();
}
