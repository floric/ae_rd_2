#include "meal.h"

Meal::Meal(int host, int guestOne, int guestTwo, double cost, int type)
{
    setHost(host);
    setGuestOne(guestOne);
    setGuestTwo(guestTwo);
    setCost(cost);
    setType(type);
}

int Meal::getGuestOne() const
{
    return guestOne;
}

void Meal::setGuestOne(int value)
{
    guestOne = value;
}

int Meal::getGuestTwo() const
{
    return guestTwo;
}

void Meal::setGuestTwo(int value)
{
    guestTwo = value;
}

int Meal::getHost() const
{
    return host;
}

void Meal::setHost(int value)
{
    host = value;
}

double Meal::getCost() const
{
    return cost;
}

void Meal::setCost(double value)
{
    cost = value;
}

int Meal::getType() const
{
    return type;
}

void Meal::setType(int value)
{
    type = value;
}

